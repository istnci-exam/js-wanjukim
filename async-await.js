//1.The async Keyword

async function fmname() {

};

fmname = async() => {

};
// async는 항상 promise를 return해준다.

withAsync = async(num) => {
    if(num === 0){
      return 'zero';
    } else{
      return 'not zero';
    }
  };

  //promise 문법
  // function withConstructor(num){
//   return new Promise((resolve, reject) => {
//     if (num === 0){
//       resolve('zero');
//     } else {
//       resolve('not zero');
//     }
//   });
// }

// withConstructor(0)
//   .then((resolveValue) => {
//   console.log(` withConstructor(0) returned a promise which resolved to: ${resolveValue}.`);
// });


//2.The await Operator
const brainstormDinner = require('./library.js');


// Native promise version:
function nativePromiseDinner() {
  brainstormDinner().then((meal) => {
	  console.log(`I'm going to make ${meal} for dinner.`);
  });
}


// async/await version:
async function announceDinner() {
  let result = await brainstormDinner();
  console.log(`I'm going to make ${result} for dinner.`);
}

announceDinner();

//3.Writing async Functions
const shopForBeans = require('./library.js');

async function getBeans() {
  console.log(`1. Heading to the store to buy beans...`);
  let value = await shopForBeans();
  console.log(`3. Great! I'm making ${value} beans for dinner tonight!`);
}

getBeans();

//4. Handling Dependent Promises
const {shopForBeans, soakTheBeans, cookTheBeans} = require('./library.js');

// Write your code below:


const makeBeans = async() => {
  const type = await shopForBeans(); //resolve값
  const isSoft = await soakTheBeans(type);
  const dinner = await cookTheBeans(isSoft);

  console.log(dinner);
}

makeBeans();


//5.Handling Errors
const cookBeanSouffle = require('./library.js');

const hostDinnerParty = async() => {
  try{
    console.log(`${await cookBeanSouffle()} is served!`);
  } catch(error) {
    console.log(error);
    console.log(`Ordering a pizza!`);
  }
}

hostDinnerParty();


//6.Handling Independent Promises
let {cookBeans, steamBroccoli, cookRice, bakeChicken} = require('./library.js');

const serveDinner = async() => {
  const vegetablePromise = steamBroccoli();
  const starchPromise = cookRice();
  const proteinPromise = bakeChicken();
  const sidePromist = cookBeans();    //개별로 await 할당하지 않아도 console.log시에 await를 붙여줘도 됨

  console.log(`Dinner is served. We're having ${await vegetablePromise}, 
  ${await starchPromise},
   ${await proteinPromise}, 
   and ${await sidePromise}.`);
}

serveDinner(); 

//7.Await Promise.all() 배열에 대해서 promise 처리
let {cookBeans, steamBroccoli, cookRice, bakeChicken} = require('./library.js')

// Write your code below:
async function serveDinnerAgain(){
  let foodArray = await Promise.all([steamBroccoli(), cookRice(), bakeChicken(), cookBeans()]); 
  
let cookBeans = foodArray[0];
let steamBroccoli =  foodArray[1];
let cookRice =  foodArray[2];
let bakeChicken =  foodArray[3];

 console.log(`Dinner is served. We’re having ${cookBeans}, ${steamBroccoli}, ${cookRice}, and ${bakeChicken}. eg. ‘Dinner is served. We’re having broccoli, rice, chicken, and beans.`)
}

serveDinnerAgain();









