//1.Function Declarations
function getReminder(){
    console.log('Water the plants');
  }
  
  function greetInSpanish(){
    console.log('Buenas Tardes.');
  }

//2.Calling a Function
function sayThanks(){
    console.log('Thank you for your purchase! We appreciate your business.');
  }
  
  sayThanks();
  sayThanks();
  sayThanks();

//3.Parameters and Arguments
function sayThanks(name) {
    console.log('Thank you for your purchase '+ name + '! We appreciate your business.');
  }
  sayThanks('Cole');

//4.Default Parameters
function makeShoppingList(item1 = 'milk', item2 = 'bread', item3 = 'eggs'){
    console.log(`Remember to buy ${item1}`);
    console.log(`Remember to buy ${item2}`);
    console.log(`Remember to buy ${item3}`);
  }
  
  makeShoppingList();

//5.Return
function monitorCount(rows, columns){
    return rows*columns;   //이 function이 리턴값을 갖게 된다.
  }
  
  const numOfMonitors = monitorCount(5,4);
  console.log(numOfMonitors);

//6.Helper Functions
function monitorCount(rows, columns) {
    return rows * columns;
  }
  
  function costOfMonitors(rows, columns) {
    return monitorCount(rows, columns) * 200;
  }
  
  const totalCost = costOfMonitors(5,4);
  
  console.log(totalCost);

//7.Function Expressions
const plantNeedsWater //펑션 표현법
= function(day){
  if(day === 'Wednesday'){
    return true;
  }else{
    return false;
  }
};

console.log(plantNeedsWater('Tuesday'));

//8.Arrow Functions
const plantNeedsWater = (day) => {
    if (day === 'Wednesday') {
      return true;
    } else {
      return false;
    }
  };

  const plantNeedsWater = function(day){
    if (day === 'Wednesday') {
      return true;
    } else {
      return false;
    }
  };

//9.Concise Body Arrow Functions
const plantNeedsWater = (day) => {
    return day === 'Wednesday' ? true : false;
  };

const plantNeedsWater2 = day => day === 'Wednesday' ? true : false;  