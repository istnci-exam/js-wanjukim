//2021.04.05 WJKIM

//1.Console
console.log('26');
console.log('10');

//2.Coment
//Openig line
console.log('It was love at first sight.');
/*
console.log('The first time Yossarian saw the chaplain he fell madly in love with him.');
console.log('Yossarian was in the hospital with a pain in his liver that fell just short of being jaundice.');
console.log('The doctors were puzzled by the fact that it wasn\'t quite jaundice.');
console.log('If it became jaundice they could treat it.');
console.log('If it didn\'t become jaundice and went away they could discharge him.');
console.log('But this just being short of jaundice all the time confused them.');

*/

//3.Data Types
console.log('JavaScript');
console.log(2011);
console.log('Woohoo! I love to code! #codecademy');
console.log(20.49);

//4.Arithmetic Operators
console.log(26 + 3.5);
console.log(2021 - 1969);
console.log(65/240);
console.log(0.2708*100);

//5.String Concatenation
console.log('Hello'+'World');
console.log('Hello'+' '+'World');

//6.Properties
console.log('Teaching the world how to code'.length);

//7. Methods
// Use .toUpperCase() to log 'Codecademy' in all uppercase letters
console.log('Codecademy'.toUpperCase());

// Use a string method to log the following string without whitespace at the beginning and end of it.
console.log('    Remove whitespace   '.trim());

//8.Built-in Objects
console.log(Math.random());
console.log(Math.random()*100);
console.log(Math.floor(Math.random()*100)); //어떤 수보다 크지 않은 최대의 정수
console.log(Math.ceil(43.8));               //어떤 수보다 작지 않은 최소의 정수
console.log(Number.isInteger(2017));        // true/false 반환

