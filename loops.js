//1.Repeating Tasks Manually
const vacationSpots = ['busan', 'sokcho', 'naju'];

console.log(vacationSpots[0]);
console.log(vacationSpots[1]);
console.log(vacationSpots[2]);

//2.The For Loop
for( let cnt = 5; cnt < 11; cnt++ ) {
    console.log(cnt);
  }

//3.Looping in Reverse
for (let counter = 3; counter >= 0
    ; counter--){
      console.log(counter);
    }

//4. Looping through Arrays
const vacationSpots = ['Bali', 'Paris', 'Tulum'];

for( let i = 0; i<vacationSpots.length; i++){
  // console.log('I would love to visit');
  // console.log(vacationSpots[i]);
  console.log(`I would love to visit ${vacationSpots[i]}`);
}

//5.Nested Loops
const bobsFollowers = ['john', 'wanju', 'tom', 'lina'];
const tinasFollowers = ['kevin', 'wanju', 'lina'];

const mutualFollowers = [];

for (let i=0; i< bobsFollowers.length; i++){
  for (let j=0; j< tinasFollowers.length; j++){
    if(bobsFollowers[i] === tinasFollowers[j]){
      mutualFollowers.push(bobsFollowers[i]);
    }
  }
}

console.log(mutualFollowers);


//6.The While Loop
const cards = ['diamond', 'spade', 'heart', 'club'];
let currentCard; 
while (currentCard != 'spade'){
  currentCard = cards[Math.floor(Math.random() * 4)];
  console.log(currentCard);
}

//7.Do...While Statements
let countString = '';
let i = 0;
 
do {
  countString = countString + i;
  i++;
} while (i < 5);
 
console.log(countString);

let cupsOfSugarNeeded = 5;
let cupsAdded = 0;

do{
  cupsAdded++
   console.log(cupsAdded + ' cup was added') 
} while (cupsAdded < cupsOfSugarNeeded)

//8.The break Keyword
const rapperArray = ["Lil' Kim", "Jay-Z", "Notorious B.I.G.", "Tupac"];

for ( let i = 0; i<rapperArray.length; i++){
  if( rapperArray[i] === 'Notorious B.I.G.' )
  {break;}
  console.log(rapperArray[i]);
  console.log('Notorious B.I.G.');
}

console.log("And if you don't know, now you know.");



