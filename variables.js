//2021.04.05 WJKIM

/*var는 유연한 변수 선언으로 테스트에는 편리할 수 있으나,
코드량이 많아진다면 사용처 파악이 힘들고 값이 바뀔 우려가 있다
=> ES6이후 let과 const가 추가되었다.
(1)let: 변수에 재할당이 가능하다.
(2)const: 변수 재선언, 재할당이 불가능하다. */

//1.Create a Variable: var
var favoriteFood = 'pizza';
var numOfSlices = 8;
console.log(favoriteFood);
console.log(numOfSlices);

//2.Create a Variable: let
let changeMe = true;
changeMe = false;
console.log(changeMe);  //reassigned

//3.Create a Variable: const
const entree = 'Enchiladas';
console.log(entree);
entree = 'Tacos'; //cannot be reassigned. (constant) 

//4.Mathematical Assignment Operators
let levelUp = 10;
let powerLevel = 9001;
let multiplyMe = 32;
let quarterMe = 1152;

// Use the mathematical assignments in the space below:

levelUp+=5;
powerLevel-=100;
multiplyMe*=11;
quarterMe/=4;

// These console.log() statements below will help you check the values of the variables.
// You do not need to edit these statements. 
console.log('The value of levelUp:', levelUp); 
console.log('The value of powerLevel:', powerLevel); 
console.log('The value of multiplyMe:', multiplyMe); 
console.log('The value of quarterMe:', quarterMe);

//5.The Increment and Decrement Operator
let gainedDollar = 3;
let lostDollar = 50;

gainedDollar++;
console.log(gainedDollar);

lostDollar--;
console.log(lostDollar);

//6.String Concatenation with Variables
const favoriteAnimal = 'hamster';
console.log('My favorite animal:'+favoriteAnimal);   // Way1
console.log(`My favorite animal:${favoriteAnimal}`); // Way2

//7.String Interpolation
const myName = 'wanju';
const myCity = 'Seong-Nam';
console.log(`My name is ${myName}. My favorite city is ${myCity}`);

//8.typeof operator(변수의 타입을 반환)
let newVariable = 'Playing around with typeof.';

console.log(typeof newVariable);

newVariable = 1;
console.log(typeof newVariable);

newVariable = 'Daymons year';
console.log(typeof newVariable);






